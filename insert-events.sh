#!/bin/bash
ELASTICSEARCH_API_ROOT="http://localhost:9200/"
LOGSTASH_API_ROOT_SHELL="http://localhost:9603"
TEMPLATE_INFO="_template/template_1"
TEMPLATE_PATH="template.json"

#Set template
curl -H 'Content-Type: application/json' -X PUT -d @${TEMPLATE_PATH} ${ELASTICSEARCH_API_ROOT}${TEMPLATE_INFO}?include_type_name=true
## POST data to Elasticsearch
if [ ! -z "$1" ]
then
    ## insert mocked application logs	
	cd "$1"
	for FILE in *-app-logs.json
	do
	  while read -r LINE || [ -n "$LINE" ];
	  do
		LINE=$(echo "$LINE")
		curl -X POST -d "$LINE" "${LOGSTASH_API_ROOT_SHELL}"
	  done < $FILE
	done
else
    echo "The app logs directory was not provided."
fi
